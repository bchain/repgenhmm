/*
 * infer_funcs.h
 *
 *  Created on: Feb 22, 2015
 *      Author: yuvalel
 */

#ifndef INFER_FUNCS_H_
#define INFER_FUNCS_H_

#include <vector>

#include "Aligner.h"
#include "ModelallVJ.h"
#include "Model_1VJ.h"

using std::tuple;
using std::string;
using std::pair;
using std::unordered_map;
using std::vector;

/// Modified Baum-Welch for calculating marginal counts for one beta chain with fixed V D J
/**
 * \returns probability for this sequence
 *
 * \param seq The sequence
 * \param V Gemonic V sequence
 * \param D Gemonic D sequence
 * \param J Gemonic J sequence
 * \param VonS Position on sequence where first nt of V is aligned to. may be negative, if first sequence nt is aligned to middle of V
 * \param JonS Position on sequence where first nt of J is aligned to
 * \param M Current model used to calculate probabilities
 * \param mar_res Object for saving the calculated marginals
*/
double BW_1seq_VDJ(const string& seq, const string& V, const string& D, const string& J,
		const int VonS, const int JonS, const Model_1VJ& M, Model_1VJ& mar_res, bool debug=false);

/// Modified Baum-Welch for calculating marginal counts for one alpha chain with fixed V J
/**
 * \returns probability for this sequence
 *
 * \param seq The sequence
 * \param V Gemonic V sequence
 * \param J Gemonic J sequence
 * \param VonS Position on sequence where first nt of V is aligned to. may be negative, if first sequence nt is aligned to middle of V
 * \param JonS Position on sequence where first nt of J is aligned to
 * \param M Current model used to calculate probabilities
 * \param mar_res Object for saving the calculated marginals
*/
double BW_1seq_VJ(const string& seq, const string& V, const string& J,
		const int VonS, const int JonS, const Model_1VJ& M, Model_1VJ& mar_res, bool debug=false);

/// Calculating marginal counts for one sequence over all possible genomic alignment choices
/**
 * \returns Probability for this sequence
 *
 * \param seq The sequence
 * \param V_alig all V alignments for sequence
 * \param J_alig all J alignments for sequence
 * \param mod Current model used to calculate probabilities
 * \param mar Object for saving the calculated marginals
 */
double marg_prob_1sq (const string& seq,
		const vector<Alignment_data>& V_alig, const vector<Alignment_data>& J_alig,
		const Model_allVJ& mod, Model_allVJ& mar, bool dbg=false);

/// Calculating marginal counts for all sequences over all possible genomic alignment choices
/**
 * \returns Probability and marginal counts (not normalized) for this sequence
 *
 * \param indexed_seq_list List of sequences
 * \param V_indexed_alignments all V alignments for all sequences
 * \param J_indexed_alignments all J alignments for all sequences
 * \param mod Current model used to calculate probabilities
 */

pair<Model_allVJ,vector<double>> marg_prob_allsq (const vector<pair<const int,const string>>& indexed_seq_list,
		const unordered_map<int,vector<Alignment_data>>& V_indexed_alignments,
		const unordered_map<int,vector<Alignment_data>>& J_indexed_alignments,
		const Model_allVJ& mod, const string& features, std::ostream& strm, bool dbg=false);

/// Calculating marginal counts for all sequences over all possible genomic alignment choices, while outputting some features for every sequence
/**
 * \returns Probability and marginal counts (not normalized) for this sequence
 *
 * \param indexed_seq_list List of sequences
 * \param V_indexed_alignments all V alignments for all sequences
 * \param J_indexed_alignments all J alignments for all sequences
 * \param features List of features to output for every sequence
 * \param strm Output stream for sequence specific features
 * \param mod Current model used to calculate probabilities
 */

pair<Model_allVJ,vector<double>> marg_prob_allsq (const vector<pair<const int,const string>>& indexed_seq_list,
		const unordered_map<int,vector<Alignment_data>>& V_indexed_alignments,
		const unordered_map<int,vector<Alignment_data>>& J_indexed_alignments,
		const Model_allVJ& mod, bool dbg=false);

//Model_allVJ initialize_model();

#endif /* INFER_FUNCS_H_ */
