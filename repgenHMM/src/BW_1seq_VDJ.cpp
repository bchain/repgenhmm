/*
 * BW_1seq_VJ.cpp
 *
 *  Created on: Dec 2, 2014
 *      Author: yuvalel
 */

#include "Model_1VJ.h"
#include <string>
#include <vector>
#include <array>
#include <iostream>
using namespace std;

double BW_1seq_VDJ(const string& seq, const string& V, const string& D, const string& J,
		const int VonS, const int JonS, const Model_1VJ& M, Model_1VJ& marginals, bool dbg) {

	const int max_seq = 500, max_state = 28000;//5000;


	if (dbg){
		cout << "debug mode" << endl;
	}
	double err = M.error_rate;
	auto matchGen = [err] (char a, char b) {return (a==b ? 1-err : err);}; //function to return the correct prob for genomic match/no-match

	int lenS = seq.length(); //length of sequence
	int lenV = V.length(); //length of V gene
	int lenJ = J.length(); //length of J gene
	int lenD = D.length(); //length of J gene

	//auto indexD = [lenD] (int d, int l, int r) {return d-l+1 + l*(2*lenD+3-l)/2 + r*(11-6*r+(r^2)+12*lenD-3*r*lenD+3*(lenD^2))/6;};
	auto indexD = [lenD] (int d, int l, int r) {return d-l + l*lenD + r*lenD*lenD;};

	int strS = max(0, VonS); //place alignment starts on sequence (either at the start or at VonS if it is positive)
	int strV = max(0, -VonS); //place alignment starts on V gene (either at the start or at -VonS if it is negative)

	int VendS = VonS + lenV; //one place after last nt of v gene is on sequence
	int endS = min(lenS, lenJ + JonS); //on sequence - one place after where alignment with J gene ends (either at the end or before, if J gene ends)
	int endJ = min(lenJ, lenS - JonS); //on J - one place after where alignment with sequence ends (either at the end or before, if sequence ends)

	int num_states = endS +4;

	// Forward algorithm
	vector<double> fwd(max_seq * max_state);

	int v = strV; // %current possible v. init as the only possible V state for step n=0
	int j = strS - (JonS + 4); // j of the only possible J from which the sequence will end at J(endJ) for step n=0

	int n = strS; //current state position

	fwd[n*max_state + sV + strV] =  matchGen(seq.at(n), V.at(v));
	for (n++,v++,j++ ; n<num_states; n++,v++,j++){
		if ((n<VendS) && (n<lenS)) //if still in V segment and in sequence
			fwd[n*max_state + sV + v] = matchGen(seq.at(n),V.at(v)) * M.trans.T(sV,v,sV,v-1) * fwd[(n-1)*max_state + sV + v-1];

		if (n>=(VendS - M.maxVdel)){ //if passed maximum V del
			if (v<=lenV) //if still in V segment or just one after
			{ //V ghost state can come only from one V state due to pre-alig
				fwd[n*max_state + sG + 1] = M.trans.T(sG,1,sV,v-1) * fwd[(n-1)*max_state + sV + v-1];
			}
			if ((v<=lenV+1) && (n<(num_states-2))) //if still in V segment or up to two after and 1st ghost was already visited
			{
				fwd[n*max_state + sG + 2] = M.trans.T(sG,2,sG,1) * fwd[(n-1)*max_state + sG + 1]; //2nd ghost state can come from 1st ghost states (and from ins, see down)
			}
			if ((v<=lenV+1) && (n<(num_states-3))) //if still in V segment or up to two after and 1st ghost was already visited
			{
				fwd[n*max_state + sI +  0] = M.nt_biasVD.at(seq.at(n-1)) * M.trans.T(sI,0,sG,1) * fwd[(n-1)*max_state + sG + 1]; //Ivd(0) can come only from G1
			}

			if (n<(num_states-3)){ //don't do insertions if last position
				for (int i=1; i<=M.maxIns; i++) //running over possible number of insertions at this point (I(max) stores max number of insertions)
				{ //given that getting out of V was possible first when v>(Vlen - maxVdel)
					fwd[n*max_state + sI + i] = M.nt_biasVD.at(seq.at(n-1)) *  M.trans.T(sI,i,sI,i-1) * fwd[(n-1)*max_state + sI +  i-1]; //I(i>1) can come only from I(i-1)
					fwd[n*max_state + sG + 2] = fwd[n*max_state + sG + 2] + M.trans.T(sG,2,sI,i-1) * fwd[(n-1)*max_state + sI +  i-1]; //2nd ghost state can also come from any ins state that was possible in the last step
				}

				fwd[n*max_state + sG + 2] = fwd[n*max_state + sG + 2] + M.trans.T(sG,2,sI,M.maxIns-1) * fwd[(n-1)*max_state + sI +  M.maxIns-1];
			}



			if ((n<(num_states-2)) && (n>1))
			{
				fwd[n*max_state + sG + 3] = 0;
				for (int l=0;l<D.length();l++){
					for (int r=0;r<D.length()-l;r++){
						if (l+r >= D.length()){ //if D is all deleted then go to next ghost
							fwd[n*max_state + sG + 3] = fwd[n*max_state + sG + 3] + M.trans.T(sD,0,l,r,sG,2) * fwd[(n-1)*max_state + sG + 2];
						}
						else{
							fwd[n*max_state + sD + indexD(l,l,r)] = matchGen(seq.at(n-2),D.at(l)) * M.trans.T(sD,0,l,r,sG,2) * fwd[(n-1)*max_state + sG + 2];
							for (int d=l+1; d<D.length()-r; d++){
								fwd[n*max_state + sD + indexD(d,l,r)] = matchGen(seq.at(n-2),D.at(d)) * 1 * fwd[(n-1)*max_state + sD + indexD(d-1,l,r)];

							}
							int d=D.length()-r-1;
							fwd[n*max_state + sG + 3] =  fwd[n*max_state + sG + 3] + 1 * fwd[(n-1)*max_state + sD + indexD(d,l,r)];
						}
					}}}

			if ((n<(num_states-1)) && (n>2))
			{
				fwd[n*max_state + sG + 4] = M.trans.T(sG,4,sG,3) * fwd[(n-1)*max_state + sG + 3]; //2nd ghost state can come from 1st ghost states (and from ins, see down)
				fwd[n*max_state + sN +  0] = M.nt_biasDJ.at(seq.at(n-3)) * M.trans.T(sN,0,sG,3) * fwd[(n-1)*max_state + sG + 3]; //I(0) can come only from G4
			}

			if (n<(num_states-2) && (n>2)){ //don't do insertions if last position
				for (int i=1; i<=M.maxIns; i++) //running over possible number of insertions at this point (I(max) stores max number of insertions)
				{ //given that getting out of V was possible first when v>(Vlen - maxVdel)
					fwd[n*max_state + sN + i] =  M.nt_biasDJ.at(seq.at(n-3)) *  M.trans.T(sN,i,sN,i-1) * fwd[(n-1)*max_state + sN +  i-1]; //I(i>1) can come only from I(i-1)
					fwd[n*max_state + sG + 4] =  fwd[n*max_state + sG + 4] + M.trans.T(sG,4,sN,i-1) * fwd[(n-1)*max_state + sN +  i-1]; //2nd ghost state can also come from any ins state that was possible in the last step
				}

				fwd[n*max_state + sG + 4] =  fwd[n*max_state + sG + 4] + M.trans.T(sG,4,sN,M.maxIns-1) * fwd[(n-1)*max_state + sN +  M.maxIns-1];
			}


			if ((n == JonS+4) && (n>3)) //if we have reached first J position
			{
				//j = 0; //first J can come only from G2
				fwd[n*max_state + sJ +  j] =  matchGen(seq.at(n-4),J.at(j)) * M.trans.T(sJ,0,sG,4) * fwd[(n-1)*max_state + sG + 4];
			}
			else if ((n > JonS+4) && (n>3)) // inside J segment
			{
				//j++; //non-first J can come from G2 or J
				fwd[n*max_state + sJ +  j] =  matchGen(seq.at(n-4),J.at(j)) * (M.trans.T(sJ,j,sJ,j-1) * fwd[(n-1)*max_state + sJ +  j-1] + M.trans.T(sJ,j,sG,4) * fwd[(n-1)*max_state + sG + 4]);
			}
		}
		if (dbg){
			double sum = 0;
			for (int k=0;k<max_state;k++) sum += fwd[n*max_state + k];
			cout << "forwards"<< n << ":" << sum << endl;
		}
	}

	if (dbg) cout << "*************"<< endl;

	// Backward algorithm
	vector<double> bwd(max_seq * max_state);

	n = num_states-1; //current position on sequence
	v = n - VonS; // %current possible v. init as the only possible V state for step n=0
	j = endJ-1; // %current possible j


	bwd[n*max_state + sJ +  j] =  1;
	for (n--,v--,j--; n>=strS; n--,v--,j--)
	{
		if ((j>=0) && (n>2)) //if current state can be J
		{
			bwd[n*max_state + sJ +  j] = matchGen(seq.at(n-4 + 1), J.at(j+1)) * M.trans.T(sJ,j+1,sJ,j) * bwd[(n+1)*max_state + sJ +  j+1];
		}
		if ((j>=-1) && (n>2)) //if next state can be J
		{
			bwd[n*max_state + sG + 4] = matchGen(seq.at(n-4 + 1), J.at(j+1)) * M.trans.T(sJ,j+1,sG,4) * bwd[(n+1)*max_state + sJ +  j+1];
		}
		if (n<num_states-2 && n>1){
			int i = M.maxIns;
			bwd[n*max_state + sN +  i] =  bwd[(n+1)*max_state + sG + 4]; //last I state can only move to G2
			for (i--; i>=0 ;i--) // insertion states can pass either to next state or to G2 state
				bwd[n*max_state + sN +  i] =  M.trans.T(sG,4,sN,i) * bwd[(n+1)*max_state + sG + 4] + M.nt_biasDJ.at(seq.at(n-3 + 1)) * M.trans.T(sN,i+1,sN,i) * bwd[(n+1)*max_state + sN +  i+1];
			bwd[n*max_state + sG + 3] =  M.trans.T(sG,4,sG,3) * bwd[(n+1)*max_state + sG + 4] + M.nt_biasDJ.at(seq.at(n-3 + 1)) * M.trans.T(sN,0,sG,3) * bwd[(n+1)*max_state + sN +  0];
		}

		if (n<num_states-3 && n>0){
			bwd[n*max_state + sG + 2] =  0;
		for (int l=0;l<D.length();l++){
			for (int r=0;r<D.length()-l;r++){
				if (l+r >= D.length()) //if D is all deleted then go to next ghost
					bwd[n*max_state + sG + 2] =  bwd[n*max_state + sG + 2] + M.trans.T(sD,0,l,r,sG,2) * bwd[(n+1)*max_state + sG + 3];
				else{
					int d = D.length() - r - 1;
					bwd[n*max_state + sD + indexD(d,l,r)] =  1 * bwd[(n+1)*max_state + sG + 3];

					for (d--; d>=l; d--){
						bwd[n*max_state + sD + indexD(d,l,r)] = matchGen(seq.at(n-2 + 1), D.at(d+1)) * 1 * bwd[(n+1)*max_state + sD + indexD(d+1,l,r)];
					}
					//G3 can lead only to Dl
					bwd[n*max_state + sG + 2] =  bwd[n*max_state + sG + 2] + matchGen(seq.at(n-2 + 1), D.at(l)) * M.trans.T(sD,0,l,r,sG,2) * bwd[(n+1)*max_state + sD + indexD(l,l,r)];
				}
			}}
		}

		if (n<num_states-4){
			int i = M.maxIns;
			bwd[n*max_state + sI +  i] =  bwd[(n+1)*max_state + sG + 2]; //last I state can only move to G3
			for (i--; i>=0 ;i--)  // insertion states can pass either to next state or to G2 state
				bwd[n*max_state + sI +  i] =  M.trans.T(sG,2,sI,i) * bwd[(n+1)*max_state + sG + 2] + M.nt_biasVD.at(seq.at(n-1 + 1)) * M.trans.T(sI,i+1,sI,i) * bwd[(n+1)*max_state + sI +  i+1];
			bwd[n*max_state + sG +  1] =  M.trans.T(sG,2,sG,1) * bwd[(n+1)*max_state + sG + 2] + M.nt_biasVD.at(seq.at(n-1 + 1)) * M.trans.T(sI,0,sG,1) * bwd[(n+1)*max_state + sI +  0];
		}



		if (n == VendS-1)
		{
			bwd[n*max_state + sV + lenV-1] = bwd[(n+1)*max_state + sG +  1];
		}
		if ((n < VendS-1)  && (n<lenS-1))
		{
			bwd[n*max_state + sV +  v] = M.trans.T(sG,1,sV,v) * bwd[(n+1)*max_state + sG +  1] + matchGen(seq.at(n+1), V.at(v+1)) * M.trans.T(sV,v+1,sV,v) * bwd[(n+1)*max_state + sV +  v+1];
		}
		if (dbg){
			cout << "backwards"<< n << ":" << endl;
		}
	}


	double prob = matchGen(seq.at(n+1), V.at(v+1)) * bwd[(n+1)*max_state + sV +  v+1];

//	if( prob != fwd.at(num_states-1).Pref(sJ, endJ-1) )
//	{
//		cout << prob << endl;
//		throw "Forward and Backward probabilities differ!";
//	}

	if (dbg) {
		cout << "*************"<< endl;
	}

	if (prob>0){
	v = strV; // place on V when is state 0
	j = - JonS + strS - 4; // place on J when is state 0
	for (int n=strS;n<num_states;n++, v++, j++)
	{
		if (dbg) {
			cout << "computing marginals for position "<< n << endl;
		}
		if ((v<lenV-1) && (n<lenS-1) && (n<num_states-1)){
			marginals.trans.T(sV,v+1,sV,v) = marginals.trans.T(sV,v+1,sV,v) + fwd[n*max_state + sV + v] * M.trans.T(sV,v+1,sV,v) *  matchGen(seq.at(n+1), V.at(v+1)) * bwd[(n+1)*max_state + sV +  v+1] / prob;
		}
		if ((v<lenV) && (n<lenS) && (n<num_states-1)){
			marginals.trans.T(sG,1,sV,v) = marginals.trans.T(sG,1,sV,v) + fwd[n*max_state + sV + v] * M.trans.T(sG,1,sV,v) * bwd[(n+1)*max_state + sG +  1] / prob;
			if (seq.at(n) != V.at(v))
			{
				marginals.error_rate = marginals.error_rate + fwd[n*max_state + sV + v] * bwd[n*max_state + sV + v] / prob;
			}
			else
			{
				marginals.non_error_rate = marginals.non_error_rate + fwd[n*max_state + sV + v] * bwd[n*max_state + sV + v] / prob;
			}
		}


		if ((n>0) && (n<num_states-4)){

			marginals.trans.T(sI,0,sG,1) = marginals.trans.T(sI,0,sG,1) + fwd[n*max_state + sG + 1] * M.nt_biasVD.at(seq.at(n-1 + 1)) * M.trans.T(sI,0,sG,1) * bwd[(n+1)*max_state + sI +  0] / prob;
			marginals.trans.T(sG,2,sG,1) = marginals.trans.T(sG,2,sG,1) + fwd[n*max_state + sG + 1] * M.trans.T(sG,2,sG,1) * bwd[(n+1)*max_state + sG + 2] / prob;
		}

		if ((n>1) && (n<num_states-4)){
			for (int i=0;i<=M.maxIns;i++)
			{
				if (i<M.maxIns){
					marginals.trans.T(sI,i+1,sI,i) = marginals.trans.T(sI,i+1,sI,i) + fwd[n*max_state + sI + i] * M.nt_biasVD.at(seq.at(n-1 + 1)) * M.trans.T(sI,i+1,sI,i) * bwd[(n+1)*max_state + sI + i+1] / prob;
				}
				marginals.trans.T(sG,2,sI,i) = marginals.trans.T(sG,2,sI,i) + fwd[n*max_state + sI + i] * M.trans.T(sG,2,sI,i) * bwd[(n+1)*max_state + sG + 2] / prob;

				marginals.nt_biasVD[seq.at(n-1)] = marginals.nt_biasVD[seq.at(n-1)] + fwd[n*max_state + sI + i] * bwd[n*max_state + sI + i] / prob;
			}
		}

		if ((n>2) && (n<num_states-3)){
			//marginals.pdelD.reserve(M.pdelD.size());
			for (unsigned l=0;l<D.length();l++){
				//marginals.pdelD.at(l).reserve(M.pdelD.at(l).size());
				for (unsigned r=0;r<D.length()-l;r++){
					//string cD = "D_" + to_string(l) + "_" + to_string(r);
					if (l+r>=D.length()){
						marginals.trans.T(sD,0,l,r,sG,2) = marginals.trans.T(sD,0,l,r,sG,2) + fwd[n*max_state + sG + 2] * M.trans.T(sD,0,l,r,sG,2) * bwd[(n+1)*max_state + sG + 3] / prob;

					}
					else{
						marginals.trans.T(sD,0,l,r,sG,2) = marginals.trans.T(sD,0,l,r,sG,2) + fwd[n*max_state + sG + 2] * matchGen(seq.at(n-2 + 1), D.at(l)) * M.trans.T(sD,0,l,r,sG,2) * bwd[(n+1)*max_state + sD + indexD(l,l,r)] / prob;
						//cout << "G3->" << cD << ": " << marginals.trans.T(sD,0,l,r,sG,2) << endl;
						for (unsigned d=l; d<D.length()-r; d++){
							if (seq.at(n-2)!=D.at(d)){
								marginals.error_rate = marginals.error_rate + fwd[n*max_state + sD + indexD(d,l,r)] * bwd[n*max_state + sD + indexD(d,l,r)] / prob;
							}
							else {
								marginals.non_error_rate = marginals.non_error_rate + fwd[n*max_state + sD + indexD(d,l,r)] * bwd[n*max_state + sD + indexD(d,l,r)] / prob;
							}
						}}
				}
			}
		}

		if ((n>2) && (n<num_states-2)){

			marginals.trans.T(sN,0,sG,3) = marginals.trans.T(sN,0,sG,3) + fwd[n*max_state + sG + 3] * M.nt_biasDJ.at(seq.at(n-3 + 1)) * M.trans.T(sN,0,sG,3) * bwd[(n+1)*max_state + sN +  0] / prob;
			marginals.trans.T(sG,4,sG,3) = marginals.trans.T(sG,4,sG,3) + fwd[n*max_state + sG + 3] * M.trans.T(sG,4,sG,3) * bwd[(n+1)*max_state + sG + 4] / prob;
		}

		if ((n>3) && (n<num_states-2)){
			for (int i=0;i<=M.maxIns;i++)
			{
				if (i<M.maxIns){
					marginals.trans.T(sN,i+1,sN,i) = marginals.trans.T(sN,i+1,sN,i) + fwd[n*max_state + sN + i] * M.nt_biasDJ.at(seq.at(n-3 + 1)) * M.trans.T(sN,i+1,sN,i) * bwd[(n+1)*max_state + sN + i+1] / prob;
				}
				marginals.trans.T(sG,4,sN,i) = marginals.trans.T(sG,4,sN,i) + fwd[n*max_state + sN + i] * M.trans.T(sG,4,sN,i) * bwd[(n+1)*max_state + sG + 4] / prob;

				marginals.nt_biasDJ[seq.at(n-3)] = marginals.nt_biasDJ[seq.at(n-3)] + fwd[n*max_state + sN + i] * bwd[n*max_state + sN + i] / prob;
			}
		}




		if ((j>=0) && (n<num_states-1) && (n>4)){
			marginals.trans.T(sJ,j+1,sJ,j) = marginals.trans.T(sJ,j+1,sJ,j) + fwd[n*max_state + sJ + j] * M.trans.T(sJ,j+1,sJ,j) *  matchGen(seq.at(n-4 + 1), J.at(j+1)) * bwd[(n+1)*max_state + sJ +  j+1] / prob;
		}

		if ((j>=-1) && (n<num_states-1) && (n>4)){
			marginals.trans.T(sJ,j+1,sG,4) = marginals.trans.T(sJ,j+1,sG,4) + fwd[n*max_state + sG + 4] * M.trans.T(sJ,j+1,sG,4) *  matchGen(seq.at(n-4 + 1), J.at(j+1)) * bwd[(n+1)*max_state + sJ +  j+1] / prob;
		}

		if ((j>=0) && (n>4)){
			if (seq.at(n-4)!=J.at(j)){
				marginals.error_rate = marginals.error_rate + fwd[n*max_state + sJ + j] * bwd[n*max_state + sJ + j] / prob;
			}
			else {
				marginals.non_error_rate = marginals.non_error_rate + fwd[n*max_state + sJ + j] * bwd[n*max_state + sJ + j] / prob;
			}
		}
	}
	}
	if (dbg) cout << prob << endl;

	return prob;

}

