/*
 * infer_HMM.cpp
 *
 *  Created on: Feb 25, 2015
 *      Author: yuvalel
 */

#include "infer_HMM.h"

using namespace std;

Model_allVJ infer_HMM (const vector<pair<const int,const string>>& indexed_seq_list,
		const unordered_map<int,vector<Alignment_data>>& V_indexed_alignments,
		const unordered_map<int,vector<Alignment_data>>& J_indexed_alignments,
		Model_allVJ M, const string savename, const int max_iter, const bool history, const bool dbg){

	double ll = 0;
	vector<double> P;
	Model_allVJ counter;

	if (dbg) cout << "debug model" << endl;
	for (int iter=0; (iter<max_iter) ; iter++)
	{
		double startTime = omp_get_wtime();
		cout << "iter: #" <<  iter  << endl;
		tie(counter,P) = marg_prob_allsq
				(indexed_seq_list, V_indexed_alignments, J_indexed_alignments, M, dbg);

		Model_allVJ ps_mod(Standard_Model(M.maxVdel, M.maxJdel, M.maxIns, M.Vs, M.Ds, M.Js)); //pseudo-count counter
		counter.add_m(ps_mod, 0); //adding pseudo-count

		for (double p : P) ll += std::log(p);
		cout << "log likelihood: "<< ll << endl;


		M = counter;//M.copy_vals(counter);
		M.model_from_counter();

		Standard_Model output_model_std (counter);
		if (history){
			ofstream outfile(savename  + "_iter" + to_string(iter) + ".txt");
			output_model_std.output(outfile);
			write_seq_prob(P, indexed_seq_list, savename + "_iter" + to_string(iter));
		}
		cout << "done. time taken: " << (omp_get_wtime() - startTime) << " seconds." << endl;
	}

	write_seq_prob(P, indexed_seq_list, savename);

	double H = 0;
	int num_seqs = 0;
	for (double p : P){
		if (p>0)
		{
			H += std::log2(p);
			num_seqs++;
		}
	}
	H = H / num_seqs;
	cout << "entropy: " <<  H  << endl;

	return M;
}


void write_seq_prob(vector<double> P, const vector<pair<const int,const string>>& indexed_seq_list,
		const string savename){
	ofstream outfile(savename + "_prob.csv");
	outfile<<"seq_index"<<";"<<"sequence"<<";"<<"gen_prob"<<endl;
	for(unsigned i = 0 ; i<indexed_seq_list.size() ; i++){
		outfile << indexed_seq_list[i].first<<";"<<indexed_seq_list[i].second<<";"<<P[i]<<endl;
	}
}
