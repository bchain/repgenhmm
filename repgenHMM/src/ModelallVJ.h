/*
 * ModelallVJ.h
 *
 *  Created on: Feb 17, 2015
 *      Author: yuvalel
 */

#ifndef MODELALLVJ_H_
#define MODELALLVJ_H_

#include <unordered_map>
#include <string>
#include <tuple>
#include "Transitions.h"
#include "Model_1VJ.h"
#include "StandardModel.h"
#include "hash_spec.h"

using std::unordered_map;
using std::string;
using std::pair;
using std::tuple;

class Standard_Model;

/// class for keeping probabilities/counts for all transitions and emissions and all V(D)J choices
class Model_allVJ {
public:

	ChainT chain_type;

	unordered_map<string,string>* Vs; ///< pointer for map of all genomic V
	unordered_map<string,string>* Ds; ///< pointer for map of all genomic D
	unordered_map<string,string>* Js; ///< pointer for map of all genomic J


	double error_rate; ///< number of errors appearing in genomic positions
	double non_error_rate; ///< number of non errors appearing in genomic positions

	int maxVdel, maxJdel, maxIns, minDlen;

	unordered_map<char, double> nt_biasVD; ///< map for emission probabilities for VD insertions
	unordered_map<char, double> nt_biasDJ; ///< map for emission probabilities for DJ insertions
	unordered_map<char, double> nt_bias; ///< map for emission probabilities for VJ insertions

	Transitions trans; ///< transitions object for transitions which are not gene dependent
	unordered_map<tuple<string,string,string>, Transitions> transVDJ; ///< transition object for each v,d,j combination
	unordered_map<string, Transitions> transV; ///< transition object for each v gene
	unordered_map<string, Transitions> transD; ///< transition object for each d gene
	unordered_map<string, Transitions> transJ; ///< transition object for each j gene
	unordered_map<tuple<string,string,string>, double> pVDJ; ///< probabilities/counts for each v,d,j combination

	unordered_map<string, int> lenV;
	unordered_map<string, int> lenD;
	unordered_map<string, int> lenJ;

	/// default empty constructor, doesn't create a usable object by itself
	Model_allVJ();
	Model_allVJ(ChainT ct);
	/// convert constructor, creates a model out of a standard model
	Model_allVJ(Standard_Model SM);

	/// adds to the current model object another model object, field by field, multiplied by factor
	void add_m(const Model_allVJ& m, double factor=1);
	/// adds to the current model object a V(D)J specific model object, field by field, multiplied by factor
	void add_m(const Model_1VJ& m, string v, string d, string j, double factor=1);
	/// multiplies current model object by factor, field by field
	void mult(double factor);
	/// Compares current model object to another according to threshold
	bool compare(const Model_allVJ& m, double thrs);
	/// restricts full transition model to V(D)J specific one
	Model_1VJ restrict(const string& v, const string& d, const string& j) const;
	/// normalize a model object used as counter. required before converting to standard model
	void model_from_counter();


	/// Writes model to stream
	void write(std::ostream& strm);
	/// Reads model from stream
	void read(std::istream& strm);

};



#endif /* MODELALLVJ_H_ */
