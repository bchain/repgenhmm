/*
 * infer_HMM.h
 *
 *  Created on: Mar 16, 2015
 *      Author: elhanati
 */

#ifndef SRC_INFER_HMM_H_
#define SRC_INFER_HMM_H_

#include <tuple>
#include <vector>
#include <limits>
#include <cmath>

#include "Aligner.h"
#include "infer_funcs.h"

/// Inferring model from sequences
/**
 * \returns transition model (normalized) inferred from sequences
 *
 * \param indexed_seq_list List of sequences
 * \param V_indexed_alignments all V alignments for all sequences
 * \param J_indexed_alignments all J alignments for all sequences
 * \param M Initial model
 * \param savename Path and name for saving intermediate models and probabilities of sequences
 * \param max_itr Number of iterations to run
 * \param history Flag whether to output model and probabilities after each iteration
 */

Model_allVJ infer_HMM (const vector<pair<const int, const string>>& indexed_seq_list,
		const unordered_map<int,vector<Alignment_data>>& V_indexed_alignments,
		const unordered_map<int,vector<Alignment_data>>& J_indexed_alignments,
		Model_allVJ M, const string savename, const int max_iter, const bool history, const bool dbg=false);

void write_seq_prob(vector<double> P, const vector<pair<const int,const string>>& indexed_seq_list,
		const string savename);

#endif /* SRC_INFER_HMM_H_ */
