/*
 * StandardModel.h
 *
 *  Created on: Feb 27, 2015
 *      Author: yuvalel
 */

#ifndef SRC_STANDARDMODEL_H_
#define SRC_STANDARDMODEL_H_

#include <unordered_map>
#include <list>
#include <vector>
#include <tuple>
#include <string>
#include <iostream>
#include "ModelallVJ.h"
#include "hash_spec.h"

using std::unordered_map;
using std::string;
using std::vector;
using std::pair;
using std::tuple;

class Model_allVJ;

const std::string nts = "ACGT";
/// Standard model class used for input and output
class Standard_Model {
public:

	ChainT chain_type;

	unordered_map<string,string>* Vs; ///< pointer for map of all genomic V
	unordered_map<string,string>* Ds; ///< pointer for map of all genomic D
	unordered_map<string,string>* Js; ///< pointer for map of all genomic J

	int maxVdel;
	int maxJdel;
	int minDlen;
	int maxIns;

	unordered_map<tuple<string,string,string>, double> pVDJ; ///< Probabilities for each V,D,J combination
	unordered_map<string, vector<double>> pdelV; ///< Deletion probabilities for each V gene
	unordered_map<string, vector<vector<double>>> pdelD; ///< Deletion probabilities (L/R) for each D gene
	unordered_map<string, vector<double>> pdelJ; ///< Deletion probabilities for each j

	vector<double> pins; ///< VJ insertion probabilities
	vector<double> pinsVD; ///< VD insertion probabilities
	vector<double> pinsDJ; ///< DJ insertion probabilities

	unordered_map<char, double> nt_biasVD; ///< map for emission probabilities for VD insertions
	unordered_map<char, double> nt_biasDJ; ///< map for emission probabilities for DJ insertions
	unordered_map<char, double> nt_bias; ///< map for emission probabilities for VJ insertions

	double error_rate; ///< probability of errors appearing in genomic positions
	double error_count; ///< mean number of errors appearing in genomic positions
	double genomic_count; ///< mean number of genomic positions


	/// output model to stream
	/**
	 * @param strm output stream, usually file stream or cout
	 * @param output_features if not empty string, outputs only certain features
	 */
	void output(std::ostream& strm, string output_features="");
	/// read model from file stream
	void read(std::istream& strm);

	/// default empty constructor, doesn't create a usable object by itself
	Standard_Model();
	/// standard constructor, creates a uniform or all-zero model ready to be used
	/**
	 * @param maxVdel Maximum number of V deletions
	 * @param maxJdel Maximum number of J deletions
	 * @param maxIns Maximum number of insertions
	 * @param Vs pointer to map of all genomic V
	 * @param Ds pointer to map of all genomic D
	 * @param Js pointer to map of all genomic J
	 * @param uinform If true, model is uniform across all choices. if false, model has zero for all probabilities. default is true.
	 */
	Standard_Model(int maxVdel, int maxJdel, int maxIns,
			 unordered_map<string,string>* Vs,  unordered_map<string,string>* Ds,
			 unordered_map<string,string>* Js, bool uinform=true);
	/// convert constructor, creates a model out of a transition model
	Standard_Model(Model_allVJ mod); //creates an new model based on mod
	//TODO: add constructor based on file, check file is ok with loaded genomics (and save.load parameters)

	/// Auxiliary initialization function for constructors, initializes a uniform or all-zero model
	/**
	 * @param maxVdel Maximum number of V deletions
	 * @param maxJdel Maximum number of J deletions
	 * @param maxIns Maximum number of insertions
	 * @param Vs pointer to map of all genomic V
	 * @param Ds pointer to map of all genomic D
	 * @param Js pointer to map of all genomic J
	 * @param uinform If true, model is uniform across all choices. if false, model has zero for all probabilities. default is true.
	 */
	void init (int maxVdel, int maxJdel, int maxIns,
				 unordered_map<string,string>* Vs,  unordered_map<string,string>* Ds,
				 unordered_map<string,string>* Js, bool uinform=true);

	/// Generates recombined sequences from given model
	/**
	 * @param N number of seqeunes to generate
	 * @param seed1 random seed for generation
	 * @param with_err if true, sequences are generated with uniform error according to model. of false, no errors are introduced.
	 * @param strm stream to write generating scenarios to
	 * @return list of sequences and index numbers as a vector of pairs
	 */
	vector<pair<const int, const string>> generate_sequences (int N, unsigned seed1,
			bool with_err, std::ostream& strm) const;


};

#endif /* SRC_STANDARDMODEL_H_ */
