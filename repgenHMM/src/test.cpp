/*
 * test.cpp
 *
 *  Created on: 20 Dec 2015
 *      Author: elhanati
 */

#include "infer_funcs.h"
#include "Transitions.h"
#include "Model_1VJ.h"
#include "ModelallVJ.h"
#include "infer_HMM.h"
#include <iostream>
using namespace std;

void testing(string folder, ChainT chain_type, unordered_map<string,string>& Vs,
		 unordered_map<string,string>& Ds,
		 unordered_map<string,string>& Js, bool gen_ref){

	string test_pool = folder + "test/test_" + (chain_type==Light?"alpha":"beta");

	double thrs(1e-3);
	bool all_ok(true);

	cout << "Testing!" << endl;

	int ins = 40, Vdel = 40, Jdel = 30;

	// load alpha and beta sequences

	vector<pair<const int, const string>> indexed_seqlist_all = read_indexed_csv(test_pool + "_indx.csv");

	unordered_map<int,vector<Alignment_data>> v_alignments =
			read_alignments_seq_csv(test_pool + "_V_alig.csv", 0, false);
	unordered_map<int,vector<Alignment_data>> j_alignments =
			read_alignments_seq_csv(test_pool + "_J_alig.csv", 0, false);

	int N = indexed_seqlist_all.size();

	//load model
	Standard_Model std_mod(Vdel, Jdel, ins, &Vs, &Ds, &Js, true);

	ifstream modelfile(test_pool + "_model.txt");
	std_mod.read(modelfile);
	Model_allVJ mod (std_mod);

if (!gen_ref){

	ifstream input_BW_file(test_pool + "_BW_ref.txt");
	ifstream input_1seq_file(test_pool + "_1seq_ref.txt");
	ifstream input_allseq_file(test_pool + "_allseq_ref.txt");

	string tmp;

	for (int i=0;i<N;i++){
		string seq = indexed_seqlist_all[i].second;
		int id = indexed_seqlist_all[i].first;
		Alignment_data v = v_alignments[id][0];
		Alignment_data j = j_alignments[id][0];

		Model_1VJ mod_res = mod.restrict(v.gene_name,Ds.begin()->first,j.gene_name);
		Model_1VJ mar_res(chain_type);

		// run BW on sequences using one V(D)J choice and output marginals per sequence
		if (chain_type==Light) BW_1seq_VJ(seq, mod.Vs->at(v.gene_name), mod.Js->at(j.gene_name),
				v.offset, j.offset, mod_res, mar_res, false);
		if (chain_type==Heavy) BW_1seq_VDJ(seq, mod.Vs->at(v.gene_name), Ds.begin()->second, mod.Js->at(j.gene_name),
				v.offset, j.offset, mod_res, mar_res, false);

		input_BW_file >> tmp;
		Model_1VJ mar_res_ref;
		mar_res_ref.read(input_BW_file);
		bool BW_ok = mar_res.compare(mar_res_ref,thrs);
		if (!BW_ok){
			cout << "wrong results while BW processing of sequence " << id << endl;
			all_ok = false;
		}
		// run 1seq on sequences using all V(D)J alignments and output marginals per sequence
		Model_allVJ marg(chain_type);
		marg_prob_1sq(seq, v_alignments.at(id), j_alignments.at(id), mod, marg, false);
		input_1seq_file >> tmp;
		Model_allVJ marg_ref;
		marg_ref.read(input_1seq_file);
		bool seq_ok = marg.compare(marg_ref,thrs);
		if (!seq_ok){
			cout << "wrong results while 1seq processing of sequence " << id << endl;
			all_ok = false;
		}
	}

	// run all_seq on sequences using all V(D)J alignments and output marginals for all

	Model_allVJ marg;
	vector<double> P;
	tie(marg, P) = marg_prob_allsq(indexed_seqlist_all, v_alignments, j_alignments, mod, false);
	Model_allVJ marg_ref;
	marg_ref.read(input_allseq_file);

	bool seqall_ok = marg.compare(marg_ref,thrs);
	if (!seqall_ok){
		cout << "wrong results while allseq processing of sequences" << endl;
		all_ok = false;
	}

	if (!all_ok) cout << "Test results for " << (chain_type==Light?"alpha":"beta") << " chain differ from reference!";
	else cout << "Test for " << (chain_type==Light?"alpha":"beta") << " chain concluded successfully" << endl;
}


else{
	//code to write reference results
    ofstream output_BW_file(test_pool + "_BW_ref.txt");
	ofstream output_1seq_file(test_pool + "_1seq_ref.txt");
	ofstream output_allseq_file(test_pool + "_allseq_ref.txt");


	for (int i=0;i<N;i++){
		string seq = indexed_seqlist_all[i].second;
		int id = indexed_seqlist_all[i].first;
		Alignment_data v = v_alignments[id][0];
		Alignment_data j = j_alignments[id][0];

		Model_1VJ mod_res = mod.restrict(v.gene_name,Ds.begin()->first,j.gene_name);
		Model_1VJ mar_res(chain_type);

		// run BW on sequences using one V(D)J choice and output marginals per sequence
		if (chain_type==Light) BW_1seq_VJ(seq, mod.Vs->at(v.gene_name), mod.Js->at(j.gene_name),
				v.offset, j.offset, mod_res, mar_res, false);
		if (chain_type==Heavy) BW_1seq_VDJ(seq, mod.Vs->at(v.gene_name), Ds.begin()->second, mod.Js->at(j.gene_name),
				v.offset, j.offset, mod_res, mar_res, false);

		output_BW_file << "seq_id:" << id << endl;
		mar_res.write(output_BW_file);
		output_BW_file << endl;

		// run 1seq on sequences using all V(D)J alignments and output marginals per sequence
		Model_allVJ marg(chain_type);
		marg_prob_1sq(seq, v_alignments.at(id), j_alignments.at(id), mod, marg, false);
		output_1seq_file << "seq_id:" << id << endl;
		marg.write(output_1seq_file);
		output_1seq_file << endl;

	}

	// run all_seq on sequences using all V(D)J alignments and output marginals for all

	Model_allVJ marg;
	vector<double> P;
	tie(marg, P) = marg_prob_allsq(indexed_seqlist_all, v_alignments, j_alignments, mod, false);

	marg.write(output_allseq_file);


}

}



