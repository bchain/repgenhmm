/*
 * Model_1VJ.h
 *
 *  Created on: Dec 1, 2014
 *      Author: yuvalel
 */

#ifndef MODEL_H_
#define MODEL_H_

#include <unordered_map>
#include "Transitions.h"

/// class for keeping probabilities/counts for all transition and emissions for fixed V(D)J
class Model_1VJ {

public:
	double error_rate; ///< number of errors appearing in genomic positions
	double non_error_rate; ///< number of non errors appearing in genomic positions
	int maxVdel, maxJdel, maxIns, minDlen;

	ChainT chain_type;

	std::unordered_map<char, double> nt_biasVD; ///< map for emission probabilities for VD insertions
	std::unordered_map<char, double> nt_biasDJ; ///< map for emission probabilities for DJ insertions

	std::unordered_map<char, double> nt_bias; ///< map for emission probabilities for VJ insertions
	Transitions trans; ///< object for all transitions

	Model_1VJ();
	Model_1VJ(ChainT ct);
	virtual ~Model_1VJ();

	/// Adds to the current model object another V(D)J specific model object, field by field, multiplied by factor
	void add_m(const Model_1VJ& m, double factor=1);
	/// Compares current model object to another according to threshold
	bool compare(const Model_1VJ& m, double thrs);

	/// Writes model to stream
	void write(std::ostream& strm);
	/// Reads model from stream
	void read(std::istream& strm);

};

#endif /* MODEL_H_ */
