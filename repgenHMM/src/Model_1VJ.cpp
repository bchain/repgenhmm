/*
 * Model_1VJ.cpp
 *
 *  Created on: Dec 1, 2014
 *      Author: yuvalel
 */

#include "Model_1VJ.h"
#include <cmath>

using namespace std;

Model_1VJ::Model_1VJ() : error_rate(0), non_error_rate(0), maxVdel(0), maxJdel(0), maxIns(0)	{
	// TODO Auto-generated constructor stub

}

Model_1VJ::Model_1VJ(ChainT ct) : chain_type(ct), error_rate(0), non_error_rate(0), maxVdel(0), maxJdel(0), maxIns(0)	{

}

Model_1VJ::~Model_1VJ() {
	// TODO Auto-generated destructor stub
}

void Model_1VJ::add_m(const Model_1VJ& m, double factor){
	error_rate += factor * m.error_rate;
	non_error_rate += factor * m.non_error_rate;
	for(const auto& it : m.nt_bias) nt_bias[it.first] += factor * it.second;
	for(const auto& it : m.nt_biasVD) nt_biasVD[it.first] += factor * it.second;
	for(const auto& it : m.nt_biasDJ) nt_biasDJ[it.first] += factor * it.second;
	trans.add(m.trans, factor);
}

bool Model_1VJ::compare(const Model_1VJ& m, double thrs){
	bool same(true);
	same &= (abs(error_rate - m.error_rate)<thrs);
	same &= (non_error_rate - m.non_error_rate)<thrs;
	for(const auto& it : m.nt_bias) same &= ((nt_bias.at(it.first) - m.nt_bias.at(it.first))<thrs);
	for(const auto& it : m.nt_biasVD) same &= ((nt_biasVD.at(it.first) - m.nt_biasVD.at(it.first))<thrs);
	for(const auto& it : m.nt_biasDJ) same &= ((nt_biasDJ.at(it.first) - m.nt_biasDJ.at(it.first))<thrs);
	same &= trans.compare(m.trans, thrs);
	return same;
}

void Model_1VJ::write(std::ostream& strm){
	strm << (chain_type==Light?"Light":"Heavy") << endl;
	strm << error_rate << " " << non_error_rate;
	strm << endl;

	if (chain_type==Light){
		for (const auto& it : nt_bias)
			strm << it.first << " " << it.second << " ";
		strm << endl;
	}
	else{
		for (const auto& it : nt_biasVD)
			strm << it.first << " " << it.second << " ";
		strm << endl;
		for (const auto& it : nt_biasDJ)
			strm << it.first << " " << it.second << " ";
		strm << endl;
	}

	trans.write(strm);
}

void Model_1VJ::read(std::istream& strm){
	string tmp;
	strm >> tmp;
	chain_type=(tmp=="Light"?Light:Heavy);

	strm >> tmp;
	error_rate = stod(tmp);
	strm >> tmp;
	non_error_rate = stod(tmp);
	if (chain_type==Light){
		for (int i=0;i<4;i++){
			char base;
			strm >> base;
			strm >> tmp;
			nt_bias[base] = stod(tmp);
		}

	}
	else{
		for (int i=0;i<4;i++){
			char base;
			strm >> base;
			strm >> tmp;
			nt_biasVD[base] = stod(tmp);
		}
		for (int i=0;i<4;i++){
			char base;
			strm >> base;
			strm >> tmp;
			nt_biasDJ[base] = stod(tmp);
		}
	}

	trans.read(strm);
}
