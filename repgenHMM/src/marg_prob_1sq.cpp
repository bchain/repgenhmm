/*
 * marg_prob_1sq.cpp
 *
 *  Created on: Feb 18, 2015
 *      Author: yuvalel
 */

#include "ModelallVJ.h"
#include "Model_1VJ.h"
#include "infer_funcs.h"
#include <string>
#include <vector>

#include "Aligner.h"
using namespace std;

double marg_prob_1sq (const string& seq,
		const std::vector<Alignment_data>& V_alig, const std::vector<Alignment_data>& J_alig,
		const Model_allVJ& mod, Model_allVJ& mar, bool dbg){

	bool db=dbg;

	double P=0;
	for (Alignment_data v:V_alig){
		for (Alignment_data j:J_alig){
			for (pair<string,string> d: *mod.Ds){
				if (dbg) cout << "V: "<< v.gene_name << " J: "<< j.gene_name << endl;
				//if ((j.gene_name=="TRAJ21*01") & (v.gene_name=="TRAV14/DV4*01") & dbg) db=true;
				Model_1VJ mod_res = mod.restrict(v.gene_name,d.first,j.gene_name);
				Model_1VJ mar_res;
				double p=0;
				try {
					if (mod.chain_type==Light)
						p = BW_1seq_VJ(seq, mod.Vs->at(v.gene_name), mod.Js->at(j.gene_name),
								v.offset, j.offset, mod_res, mar_res, db);
					else if (mod.chain_type==Heavy)
						p = BW_1seq_VDJ(seq, mod.Vs->at(v.gene_name), d.second,  mod.Js->at(j.gene_name),
								v.offset, j.offset, mod_res, mar_res, db);
				}
				catch (exception& e){
					cout << "Exception: " << e.what() << " for seq:" << endl;
					cout << seq << endl;
					cout << "when processed for genes " << v.gene_name << " / "<< j.gene_name << endl;
					cout << endl;
				}
				catch (string& e){
					cout << "Exception: " << e << " for seq:" << endl;
					cout << seq << endl;
					cout << "when processed for genes " << v.gene_name << " / "<< j.gene_name << endl;
					cout << endl;
				}
				catch (...){
					cout << "Exception (unknown) for seq:" << endl;
					cout << seq << endl;
					cout << "when processed for genes " << v.gene_name << " / "<< j.gene_name << endl;
					cout << endl;
				}
				if (p>0){
					double VDJ_factor = mod.pVDJ.at(make_tuple(v.gene_name,d.first,j.gene_name)) * p; //need to add VJ joint prob field somewhere
					mar.add_m(mar_res, v.gene_name, d.first, j.gene_name, VDJ_factor);
					P = P + VDJ_factor; //add overall prob
					if (dbg) cout << "p: "<< p << " VDJfac: "<< VDJ_factor << endl << endl;

				}
			}
		}
	}
	mar.mult(1/P);//divide mar by overall prob
	return P;
}


